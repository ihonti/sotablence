package nextgensort

import (
	"sort"
)

func Sorting(sn *[]int) {
	sort.SliceStable(*sn, func(i, j int) bool {
		return (*sn)[i] < (*sn)[j]
	})
} 
